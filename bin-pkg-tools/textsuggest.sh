#!/usr/bin/env bash

LD_LIBRARY_PATH=/usr/share/textsuggest/libs QT_QPA_PLATFORM_PLUGIN_PATH=/usr/share/textsuggest/libs /usr/share/textsuggest/textsuggest "$@"
