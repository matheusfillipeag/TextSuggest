# TextSuggest
### Universal Autocomplete

[![TextSuggest logo](img/TextSuggest-banner.png)](https://bharadwaj-raju.itch.io/textsuggest)

## [**Download Here**](https://bharadwaj-raju.itch.io/textsuggest#download)

Autocomplete, text expansion, etc, in all GUI apps (on X11).

TextSuggest supports [multiple languages](#other-languages) and [extensions](#extensions).

![TextSuggest in action](img/demo.png)

Licensed under the [GNU GPL 3](https://www.gnu.org/licenses/gpl.txt). TextSuggest is free as in freedom.

[Acknowledgements for the dictionaries.](#dictionary-credits)

## Overview

TextSuggest is a program that shows completions for the word selected or (optionally) currently being typed.

It is generally bound to a keyboard shortcut.

## Features

Click on these for more info:

<details><summary><b>Fast</b></summary>
<p><br />
TextSuggest is extremely fast. It does a fuzzy search of the full included English dictionary in 0.0005 seconds.
</p>
</details> <br />

<details><summary><b>Universal Autocomplete</b></summary>
<p><br />
TextSuggest can provide autocomplete in any GUI app on X11.
</p>
</details> <br />

<details><summary><b>Text Expansions, and more</b></summary>
<p><br />
<p>TextSuggest can handle a range of expansions.</p>

<b>Custom words</b>

<p>Simply add them to <code>~/.config/textsuggest/custom-words.json</code> in a JSON format like this:</p>

<pre><code class="language-javascript">{
    "custom": "Expansion",
    "another": "Another expansion"
}
</code></pre>

<p>and whenever 'custom' is typed, 'Expansion' will be typed. Similarly for 'another' ('Another expansion').</p>

<p>Whenever you type an unknown word, it is automatically added to your custom words file.</p>

<b>Commands</b>

<p>Inserts the output of a command:</p>

<pre><code>$ls -l
</code></pre>

<p>when typed into a TextSuggest window, will insert output of <code>ls -l</code> as if it was run in a shell.</p>

<h4>Custom words + Commands</h4>

<p>Add in <code>~/.config/textsuggest/custom-words.json</code>:</p>

<pre><code>"custom": "$ command --opts"
</code></pre>

<p>and whenever you type 'custom' into TextSuggest, the output of <code>command --opts</code> will be inserted.</p>

<b>Math</b>

<p>Simply type into TextSuggest:</p>

<pre><code>= 2 + 3
</code></pre>

<p>And '5' will be inserted. You can do any math expression that Python supports.</p>

<p>You can also use any function in the Python <a href="https://docs.python.org/3/library/math.html"><code>math</code></a> library, for example <code>= sqrt(25)</code> for √25.</p>

<b>Custom Words + Math</b>

<p>Add in <code>~/.config/textsuggest/custom-words.json</code>:</p>

<pre><code>"custom": "= 2 + 3"
</code></pre>

<p>And whenever you type 'custom' into TextSuggest, 5 will be inserted.</p>
</p>
</details> <br />


<details><summary><b>Fuzzy Matching</b></summary>
<p><br />
TextSuggest supports very fast and intuitive fuzzy matching, so that you don't have to type the entire word, only portions.

For example, as the screenshot at the top shows, `inting` shows suggestions for `interesting`, `painting` and so on, in order of best match.
</p>
</details> <br />

<details><summary><b>Extensions</b></summary>
<p><br />
TextSuggest supports powerful *processors* for extensions.

A processor <i>processes</i> text before handing it over to TextSuggest to type it out.
By default TextSuggest has two processors, <code>command</code> and <code>math_expression</code> (see the above <i>Text Expansions</i> section).

<b>Making your own extension</b>

<p>A <em>processor</em> is a simple script/executable (any language), that <em>must</em> respond to <code>matches {text}</code> and <code>process {text}</code> as command line arguments.</p>

<p>Look into this example, written in Python (you can use any language, as long as it is an executable):</p>

<pre><code class="language-python">import sys

if sys.argv[1] == "matches":
    # Exit (sys.exit()) with 0 (yes) or 1 (no): whether this processor should process 'text' or not.
    # For example, the command processor has it like this:
    #     if text.startswith('$'):
    #         sys.exit(0)  # should process
    #     else:
    #         sys.exit(1)  # should not process</p>

if sys.argv[1] == "process":
    text = sys.argv[2]
    # Do something with 'text' and print() it.
    # This is what will be finally typed.
</code></pre>

<p>Make one based on the sample above, and place it in <code>~/.config/textsuggest/processors/</code>.</p>

<p>File must be executable (<code>chmod a+x ~/.config/textsuggest/processors/YOUR_PROCESSOR</code>).</p>

<p>Processors in <code>~/.config/textsuggest/processors</code> take precedence over those in <code>/usr/share/textsuggest/processors</code>, in case of a name or match conflict.</p>

</p>
</details> <br />

<details><summary><b>History</b></summary>
<p><br />
TextSuggest supports storing history of suggestions used. More-used suggestions will rise to the top.

History can be disabled using the <code>--no-history</code> option.

You can remove a word from history, by pressing <kbd>Shift+Delete</kbd>, or in the file <code>~/.config/textsuggest/history.json</code>
</p>
</details> <br />

<details><summary><b>"Ignore" Certain Words</b></summary>
<p><br />
You can tell TextSuggest to *never* show some words conveniently through

 - <kbd>Ctrl+Shift+Delete</kbd>
 - or in the file <code>~/.config/textsuggest/ignore.json</code>
</p>
</details> <br />

<details><summary><b>Keyboard Shortcuts</b></summary>
<p><br />
While browsing the list of suggestions, press

  - <kbd>Alt+Enter</kbd> to type it without processing.
  - <kbd>Shift+Delete</kbd> to remove it from your history.
  - <kbd>Ctrl+Shift+Delete</kbd> to add it to the ignore list (i.e. will never show up in suggestions)
</p>
</details> <br />

<details><summary><b>Native UI</b></summary>
<p><br />
Unlike many apps, TextSuggest has a fast, entirely native user interface written in Qt 5 and C++.

Custom third-party UIs can also be easily made by other developers if they wish.
</p>
</details> <br />

<details><summary><b>Multiple Languages</b></summary>
<p><br />

<i><b>All languages</b> are supported!</i>

<p>English dictionary are provided by default. Please search online for dictionaries of your language.</p>

<p>By default, only the English dictionary will be used. Use <code>--language={your_language}</code> option to change this.</p>

<p>Then TextSuggest will use <code>{your_language}.txt</code> file(s) in <code>/usr/share/textsuggest/dictionaries/</code> (if they exist).</p>

<p>For example, if you want TextSuggest to use, let us say, English and Telugu dictionaries:</p>

<pre><code class="language-bash">
$ textsuggest --language=English --language=Telugu
</code></pre>

<p>and so on (note that <code>--language=English,Telugu</code> or <code>--language English Telugu</code> will not work, you must specify <code>--language</code> flag for each).</p>

<p>TextSuggest also has an optional feature to auto-detect your language from your keyboard layout. Use <code>--auto-detect-language</code> option for this.</p>

<p>TextSuggest can auto-detect language from following keyboard layouts:</p>

<ul>
	<li><code>bd</code> → Bangla</li>
	<li><code>us</code> → English</li>
	<li><code>uk</code> → English</li>
	<li><code>gb</code> → English</li>
	<li><code>cn</code> → Chinese</li>
	<li><code>ar</code> → Arabic</li>
	<li><code>tw</code> → Chinese</li>
	<li><code>de</code> → German</li>
	<li><code>jp</code> → Japanese</li>
	<li><code>ru</code> → Russian</li>
	<li><code>es</code> → Spanish</li>
	<li><code>se</code> → Swedish</li>
	<li><code>fi</code> → Finnish</li>
	<li><code>kr</code> → Korean</li>
	<li><code>pk</code> → Urdu</li>
	<li><code>fr</code> → French</li>
	<li><code>gr</code> → Greek</li>
	<li><code>ua</code> → Ukrainian</li>
</ul>

<p><em>Even if your language is not listed above, that only means that TextSuggest will not be able to auto-detect it from your keyboard
layout! You can still specify it manually using <code>--language={your_language}</code> and it will work!</em></p>

</p>
</details>


## Installation

**Easy Install:**

  1. [Download the latest release here](https://bharadwaj-raju.itch.io/textsuggest#download), then extract it.

  2. Run:

```bash
$ cd {path to extracted files}
$ sudo sh install.sh
```

**Now, see [Usage](#usage)**

*Note: the packaged releases of TextSuggest (TextSuggest-Pkg.zip) have all dependencies bundled, so you don't have to install any libraries or compile anything!*



**Difficult Install: Building from Source:**

If you don't want that, please run `sh build.sh` then `sudo sh install.sh`.

  1. [Download the latest release (source code)](https://gitlab.com/bharadwaj-raju/TextSuggest/tags), then extract it.

  2. Run:

```bash
$ cd {path to extracted files}
$ sh build.sh
$ sudo sh install.sh
```

**Now, see [Usage](#usage)**


## Usage

Run the command `textsuggest-server` in the background, and set it to run on startup.

Set the command `textsuggest` to a keyboard shortcut. Type a word, select it, press the shortcut and TextSuggest will give you autocomplete.

This offers the most basic use of TextSuggest. For more, see [options](#options) and click on the [features](#features).

## Uninstallation

Use `sudo sh install.sh --uninstall`.

## Options

    $ textsuggest --help
	usage: textsuggest [options]

	TextSuggest — universal autocomplete

	optional arguments:

	  -h, --help            show this help message and exit

	  --word WORD [...]
	                        Specify word to give suggestions for. Default: all words.

	  --no-history          Disable the frequently-used words history (stored in ~/.config/textsuggest/history.json)

	  --language languages [...]
	                        Set language(s). Default: English. See also: --auto-detect-language.

	  --auto-detect-language
	                        Auto-detect language from keyboard layout.

	  --selection           Show suggestions for currently selected word. See also: --auto-selection

	  --auto-selection [beginning|middle|end]
	                        Automatically select word under cursor and suggest. Ignored if --no-selection.

	  --custom-words-only   Show custom words only.

	  --no-processing       Disable using of any processors.

	  -v, --version         Print version and license information.



## Dictionary Credits

- English:
  Oxford 3k wordlist (filtered to only include words with >= 5 chars)
