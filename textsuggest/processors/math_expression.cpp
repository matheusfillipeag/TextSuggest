#include <iostream>
#include <string>
#include <sstream>

#include "../../lib/subprocess.hpp"
#include "../../lib/subprocess2.hpp"

namespace sp2 = subprocess2;

int main(int argc, char ** argv) {
	
	std::vector<std::string> args(argv, argv+argc);

	if (argc < 3) {
		return 3;
	}

	std::string op = args[1];
	std::string text = args[2];

	if (op == "matches") {
		if (text.substr(0, 1) == "=") {
			return 0;
		} else {
			return 1;
		}
	} else if (op == "process") {
		text.erase(0, 1);
		std::string python_cmd = "from math import *;print(";
		python_cmd += text + ")";
		std::cerr << python_cmd << std::endl;
		auto p = sp2::check_output({"python3", "-c", python_cmd});
		std::cout << p.buf.data();
	}
	
	return 0;

}
